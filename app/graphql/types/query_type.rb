module Types
  class QueryType < Types::BaseObject
    field :todos, [Types::TodoType], null: false
    field :todo, Types::TodoType, null: false do
      argument :id, ID, required: true
    end

    def todos
      Todo.all
    end

    def todo(id:)
      User.find(id)
    end
  end
end
