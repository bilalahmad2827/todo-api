module Types
  class MutationType < Types::BaseObject
    field :create_todo, mutation: Mutations::Todos::CreateTodo
    field :delete_todo, mutation: Mutations::Todos::DeleteTodo
    field :update_todo, mutation: Mutations::Todos::UpdateTodo
  end
end
