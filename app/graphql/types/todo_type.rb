module Types
  class TodoType < Types::BaseObject
    field :id, ID, null: false
    field :body, String, null: true
    field :status, Integer, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :completed, Boolean, null: false

    def completed
      object.completed?
    end
  end
end
