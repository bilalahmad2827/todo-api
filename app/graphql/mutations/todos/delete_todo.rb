module Mutations
  module Todos
    class DeleteTodo < Mutations::BaseMutation
      argument :id, ID, required: true

      field :errors, [String], null: false

      def resolve(id:)
        todo = Todo.find(id)

        if todo.destroy
          {
            errors: []
          }
        else
          {
            errors: todo.errors.full_messages
          }
        end
      end
    end
  end
end
