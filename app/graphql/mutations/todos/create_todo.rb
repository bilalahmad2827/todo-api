module Mutations
  module Todos
    class CreateTodo < Mutations::BaseMutation
      argument :body, String, required: true
      argument :status, Integer, required: false

      field :todo, Types::TodoType, null: false
      field :errors, [String], null: false

      def resolve(**arguments)
        todo = Todo.new(arguments)

        if todo.save
          {
            todo: todo,
            errors: []
          }
        else
          {
            todo: nil,
            errors: todo.errors.full_messages
          }
        end
      end
    end
  end
end
