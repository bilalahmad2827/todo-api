module Mutations
  module Todos
    class UpdateTodo < Mutations::BaseMutation
      argument :id, Integer, required: true
      argument :body, String, required: false
      argument :status, Integer, required: false

      field :todo, Types::TodoType, null: false
      field :errors, [String], null: false

      def resolve(id:, **attributes)
        todo = Todo.find(id)

        if todo.update(attributes)
          {
            todo: todo,
            errors: []
          }
        else
          {
            todo: nil,
            errors: todo.errors.full_messages
          }
        end
      end
    end
  end
end
