class Todo < ApplicationRecord
  enum status: { pending: 0, completed: 1 }.freeze

  validates :body, presence: :true
end
